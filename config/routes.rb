RorBugtracker::Application.routes.draw do
  get "versions/create"

  get "components/create"

  get "home/index"

  resources :home, :only => :index

  devise_for :users

  match 'projects/:id/settings', :to => 'projects#settings', :via => :get
  resources :projects do
    get :autocomplete_user_email, :on => :collection
  end

  resources :users, :only => :show

  resources :user_projects
  match 'user_projects/delete/:uid/:pid', :to => 'user_projects#delete', :via => :delete

  resources :issues
  match 'projects/:id/issues', :to => 'issues#of_project', :via => :get
  match 'projects/:id/issues/new', :to => 'issues#new', :via => :get
  match 'issues/add_like/:id', :to => 'issues#add_like', :via => :get

  resources :histories

  resource :components
  match 'components/create', :to => 'components#create', :via => :post
  match 'components/delete/:id', :to => 'components#delete', :via => :get

  resource :versions
  match 'versions/create', :to => 'versions#create', :via => :post
  match 'versions/delete/:id', :to => 'versions#delete', :via => :get

  resource :comments
  match 'versions/create', :to => 'versions#create', :via => :post
  match 'comments/delete/:id', :to => 'comments#delete', :via => :get

  post 'issues/apply_filters'


  root :to => 'home#index'



  #get "projects" => "projects#index"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
