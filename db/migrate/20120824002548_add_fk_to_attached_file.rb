class AddFkToAttachedFile < ActiveRecord::Migration
  def change
    add_foreign_key(:attached_files, :issues)
  end
end
