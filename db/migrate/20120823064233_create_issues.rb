class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.string :title
      t.text :description
      t.enum :priority
      t.enum :type
      t.enum :status
      t.integer :done
      t.integer :assignee_id
      t.integer :author_id
      t.references :project

      t.timestamps
    end
  end
end
