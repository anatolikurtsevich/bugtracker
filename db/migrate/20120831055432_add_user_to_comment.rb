class AddUserToComment < ActiveRecord::Migration
  def up
    change_table :comments do |t|
      t.references :user
    end
    add_foreign_key(:comments, :users)
  end
  def down
    remove_column :comments, :user_id
    remove_foreign_key(:comments, :users)
  end
end
