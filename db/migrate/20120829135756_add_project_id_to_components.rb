class AddProjectIdToComponents < ActiveRecord::Migration
  def up
    change_table :components do |t|
      t.references :project
      add_foreign_key(:components, :projects)
    end
  end
  def down
    remove_column :components, :project_id
    remove_foreign_key(:components, :projects)
  end
end
