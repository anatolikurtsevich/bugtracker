class CreateAttachedFiles < ActiveRecord::Migration
  def change
    create_table :attached_files do |t|
      t.string :name
      t.text :path
      t.string :type
      t.references :issue

      t.timestamps
    end
  end
end
