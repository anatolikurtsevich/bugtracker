class AddFkToIssueVersion < ActiveRecord::Migration
  def change
    add_foreign_key(:issues_versions, :versions, column: 'version_id')
    add_foreign_key(:issues_versions, :issues, column: 'issue_id')
  end
end
