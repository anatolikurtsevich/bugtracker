class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.string :image
      t.string :codename
      t.date :deadline

      t.timestamps
    end
  end
end
