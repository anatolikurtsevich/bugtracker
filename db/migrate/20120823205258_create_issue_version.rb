class CreateIssueVersion < ActiveRecord::Migration
  def change
    create_table :issues_versions, :id => false do |t|
      t.references :version
      t.references :issue
    end
  end
end
