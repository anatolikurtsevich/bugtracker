class AddFkToIssue < ActiveRecord::Migration
  def change
    add_foreign_key(:issues, :users, column: 'assignee_id')
    add_foreign_key(:issues, :users, column: 'author_id')
    add_foreign_key(:issues, :projects)
  end
end
