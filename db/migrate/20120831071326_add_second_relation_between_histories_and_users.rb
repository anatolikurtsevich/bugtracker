class AddSecondRelationBetweenHistoriesAndUsers < ActiveRecord::Migration
  def up
    change_table :histories do |t|
      remove_foreign_key(:histories, :users)
      t.rename :user_id, :assignee_id
      t.integer :author_id
      add_foreign_key(:histories, :users, column: 'assignee_id')
      add_foreign_key(:histories, :users, column: 'author_id')
    end
  end
end
