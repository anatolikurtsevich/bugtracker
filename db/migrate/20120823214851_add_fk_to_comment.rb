class AddFkToComment < ActiveRecord::Migration
  def change
    add_foreign_key(:comments, :issues, column: 'issue_id')
  end
end
