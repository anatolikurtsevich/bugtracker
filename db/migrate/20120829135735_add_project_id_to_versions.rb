class AddProjectIdToVersions < ActiveRecord::Migration
  def up
    change_table :versions do |t|
      t.references :project
    end
    add_foreign_key(:versions, :projects)
  end
  def down
    remove_column :versions, :project_id
    remove_foreign_key(:versions, :projects)
  end
end
