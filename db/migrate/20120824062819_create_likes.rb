class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes do |t|
      t.references :user
      t.references :issue
      t.timestamps
    end
    add_foreign_key(:likes, :users, column: 'user_id')
    add_foreign_key(:likes, :issues, column: 'issue_id')
  end
end
