class AddImageToProjects < ActiveRecord::Migration
  def up
    change_table :projects do |t|
      t.has_attached_file :image
    end
  end

  def down
    drop_attached_file :projects, :image
  end
end
