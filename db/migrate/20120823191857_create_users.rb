class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :fname
      t.string :lname
      t.string :login
      t.string :email
      t.string :pass
      t.string :image

      t.timestamps
    end
  end
end
