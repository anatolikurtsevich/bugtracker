class CreateHistories < ActiveRecord::Migration
  def change
    create_table :histories do |t|
      t.text :message
      t.string :done
      t.enum :status
      t.enum :priority
      t.references :issue
      t.references :user

      t.timestamps
    end
  end
end
