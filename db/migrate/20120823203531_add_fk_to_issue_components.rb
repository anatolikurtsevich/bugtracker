class AddFkToIssueComponents < ActiveRecord::Migration
  def change
    add_foreign_key(:components_issues, :components, column: 'component_id')
    add_foreign_key(:components_issues, :issues, column: 'issue_id')
  end
end
