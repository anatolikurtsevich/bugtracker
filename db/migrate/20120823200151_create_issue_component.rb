class CreateIssueComponent < ActiveRecord::Migration
  def change
    create_table :components_issues, :id => false  do |t|
      t.references :component
      t.references :issue
    end
  end
end
