// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require_tree .
//= require jquery-ui
//= require autocomplete-rails



$(document).ready(function() {
    var count = 1;
    $("#add_file").bind("click", function() {
        var div = $("#files")
        count++
        if (count <= 10)
            div.append('<div><input type="file", id=' + count + ' name=' + count +' ></div>');
    })

    $("#delete_file").bind("click", function() {
        var div = $("#files")
        if (count > 0) {
            var elem = $("#" + count)
            elem.remove()
        }
    })
});