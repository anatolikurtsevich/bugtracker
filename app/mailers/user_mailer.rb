class UserMailer < ActionMailer::Base
  default :from => "test@test.com"

  def issue_assigned_email(user, issue)
    @user = user
    @issue = issue
    mail(:to => user.email, :subject => "New issue")
  end

  def deadline_failed_email(user, issue)
    @user = user
    @issue = issue
    mail(:to => user.email, :subject => "you failed, lazy son of a bitch!")
  end

end
