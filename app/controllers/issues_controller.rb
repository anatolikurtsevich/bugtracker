class IssuesController < ApplicationController
  before_filter :authenticate_user!

  def of_project
    @issues = Project.find(params[:id]).issues
    @pid = params[:id]
  end

  def apply_filters
    @issues = Project.find(params[:issue][:project_id]).issues
    if(!params[:issue][:author_id].empty?) then
      @issues = @issues.where_author(params[:issue][:author_id])
    end
    if(!params[:issue][:assignee_id].empty?) then
      @issues = @issues.where_assignee(params[:issue][:assignee_id])
    end
    if(!params[:issue][:priority].empty?) then
      @issues = @issues.where_priority(params[:issue][:priority])
    end
    if(!params[:issue][:status].empty?) then
      @issues = @issues.where_status(params[:issue][:status])
    end
    if(!params[:issue][:issue_type].empty?) then
      @issues = @issues.where_type(params[:issue][:issue_type])
    end
    @issues = @issues.all
    respond_to do |format|
      format.json { render :json => @issues }
      format.js
    end
  end

  def assigned_for
    @issues = User.find(params[:uid]).issues_assigned
  end

  def created_by
    @issues = User.find(params[:uid]).issues_created
  end

  #show issues by id
  def show
    @issue = Issue.find(params[:id])
    @author = @issue.author
    @assignee = @issue.assignee
    @comments = @issue.comments
    @files = @issue.attached_files
  end

  def new
    @issue = Project.find(params[:id]).issues.new
  end

  def create
    @issue = Issue.new(params[:issue])
    @issue.author = current_user
    @issue.status = :new
    if @issue.valid?
      @issue.save
      #UserMailer.issue_assigned_email(current_user, new_issue).deliver
      (1..10).to_a.each do |i|
        if params[i.to_s]
          uploaded_io = params[i.to_s]
          if(uploaded_io) then
            name = uploaded_io.original_filename
            path = File.join("uploads", name)
            File.open(File.join("public", path), 'wb') do |file|
              file.write(uploaded_io.read)
            end
            attachment = AttachedFile.new(:name => name, :path => path)
            attachment.issue = @issue
            attachment.save
          end
        end
      end

      redirect_to :action => :of_project, :id => params[:issue][:project_id]
    else
      render 'new'
    end
  end

  def edit
    @issue = current_user.issues_created.find(params[:id])
  end

  def update
    @issue = current_user.issues_created.find(params[:id])
    if @issue.update_attributes(params[:issues])
      redirect_to :controller => :issues, :action => :show, :id => params[:id]
    else
      render 'edit'
    end
  end

  def settings
    @issue = current_user.issues.find(params[:id])
    @versions = @issue.versions
    @components = @issue.components
    @members = @issue.users
  end

  def add_like
    unless  Like.where(:user_id => current_user.id, :issue_id => params[:id]).exists?
      Like.create(:user_id => current_user.id, :issue_id => params[:id])
    end
    @issue = Issue.find(params[:id])
    respond_to do |format|
      format.json { head :json => @issue}
      format.js #added
    end
  end
end


