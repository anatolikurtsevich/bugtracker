class HomeController < ApplicationController
  before_filter :authenticate_user!
  def index
    @p = {}
    current_user.projects.each do |p|
       @p[p] = p.issues.where({:status => :new, :assignee_id => current_user.id})
    end
  end
end
