class HistoriesController < ApplicationController
  before_filter :authenticate_user!

  #our histories
  def index
    @histories = Issue.find(params[:id]).histories
  end

  #show histories by id
  def show
    @history = history.find(params[:id])
  end

  def new
    @history = Project.find(params[:id]).histories.new
  end

  def create
    issue = Issue.find(params[:issue][:issue_id])
    new_history = issue.histories.new(issue.paramsForHistory)
    new_history.message = params[:message]
    new_history.author = current_user
    if new_history.valid?
      new_history.save

      issue.customUpdate(params[:issue]).save
    end
    #if (new_history.save and new_history.issue.save)

      redirect_to :controller => :issues, :action => :show, :id => new_history.issue_id

    #else
    #  render 'new'
    #end
  end

  def edit
    @history = History.find(params[:id])
  end

  def update

      redirect_to :controller => :home, :action => :index
    else
      render 'edit'
    end
end