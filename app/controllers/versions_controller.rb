class VersionsController < ApplicationController
  def create
    @project = Project.find(params[:versions][:project_id])
    if !(Version.where(:label => params[:versions][:label], :project_id => @project.id).exists?)
      @version = Version.new(params[:versions])
      if @version.save
        respond_to do |format|
          format.json { render :json => @version}
          format.js  #added
        end
      end
      end
  end

  def delete
    @version = Version.find(params[:id])
    @version.destroy
    respond_to do |format|
      format.json { head :no_content }
      format.js #added
    end
  end
end
