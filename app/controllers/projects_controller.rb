class ProjectsController < ApplicationController
  before_filter :authenticate_user!

  autocomplete :user, :email

  def index
    @projects = current_user.projects
  end

  def show
    @project = current_user.projects.find(params[:id])
    @managers = User.joins(:user_projects).where('role = ? AND project_id = ?', :manager, @project.id).all
    @developers = User.joins(:user_projects).where('role = ? AND project_id = ?', :assignee, @project.id).all

  end

  def new
    @project = Project.new
  end

  def create
    if current_user.projects.create(params[:project])
      redirect_to :action => :index
    else
      render 'new'
    end
  end

  def edit
    @project = current_user.projects.find(params[:id])
  end

  def update
    @project = current_user.projects.find(params[:id])
    if @project.update_attributes(params[:project])
      redirect_to :controller => :home, :action => :index
    else
      render 'edit'
    end
  end

  def destroy
    @project = current_user.projects.find(params[:id])
    @project.destroy

    redirect_to :action => :index
  end

  def settings
    @project = current_user.projects.find(params[:id])
    @versions = @project.versions
    @components = @project.components
    @members = @project.users
  end

end

