class CommentsController < ApplicationController
  def new
    @comment = Comment.new
  end

  def create
    @comment = current_user.comments.create(params[:comment])
    respond_to do |format|
      format.json { render :json => @up }
      format.js #added
    end
  end

  def edit
    @comment = Comment.find(params[:id])
  end

  def delete
    @comment = Comment.find(params[:id])
    @comment.destroy
    respond_to do |format|
      format.json { head :no_content }
      format.js #added
    end
  end
end
