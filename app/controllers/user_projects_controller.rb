class UserProjectsController < ApplicationController
  def create
    @project = Project.find(params[:user_projects][:project_id])
    @up = @project.user_projects.new()
    if  (User.where(:email => params[:user_projects][:email]).exists?)
      @up.user = User.find_by_email(params[:user_projects][:email])
      if !(UserProject.where(:user_id => @up.user.id, :project_id => @up.project.id).exists?)
        @up.save!
        @up.update_column(:role, params[:user_projects][:role])
        respond_to do |format|
          format.json { render :json => @up }
          format.js #added
        end
      end
    end
  end

  def delete
    #if Project.find(params[:pid]).author.id == params[:uid]
    #  return
    #end
    @up = UserProject.where(:user_id => params[:uid], :project_id => params[:pid]).first
    @up.destroy
    respond_to do |format|
      format.json { head :no_content }
      format.js #added
    end
  end
end
