class Version < ActiveRecord::Base
  attr_accessible :label, :project_id
  validates :label, :presence => :true,:length => { :maximum => 60, :too_long => "%{count} characters is the maximum allowed" }
  has_and_belongs_to_many :issues
  belongs_to :project
end
