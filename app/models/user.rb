class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable#, :confirmable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  attr_accessible :fname, :image, :lname, :login, :pass , :avatar

  validates :login, :uniqueness => true

  has_many :user_projects
  has_many :projects,
           :through => :user_projects

  has_many :issues_created,
           :class_name => "Issue",
           :foreign_key => "author_id"

  has_many :issues_assigned,
           :class_name => "Issue",
           :foreign_key => "assignee_id"

  has_many :histories_created,
           :class_name => "Issue",
           :foreign_key => "author_id"

  has_many :histories_assigned,
           :class_name => "Issue",
           :foreign_key => "assignee_id"

  has_many :comments

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>", :small => "40x40>" }

end
