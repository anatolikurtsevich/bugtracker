class AttachedFile < ActiveRecord::Base
  attr_accessible :name, :path, :type
  validates :name, :presence => true
  validates :path, :presence => true
  belongs_to :issue


end
