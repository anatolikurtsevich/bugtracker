class UserProject < ActiveRecord::Base
  attr_accessible :role
  enum_attr :role, %w(^manager assignee)

  belongs_to :user
  belongs_to :project

  before_validation do |user_project|
    user_project.role = :manager
  end
end
