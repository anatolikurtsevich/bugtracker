namespace :sendmail do
  task :to_fools => :environment do
    Issue.all.each do |issue|
      if(issue.deadline) then
        if(DateTime.now() > issue.deadline) then
        UserMailer.deadline_failed_email(issue.assignee, issue).deliver
        end
      end
    end

  end
end
